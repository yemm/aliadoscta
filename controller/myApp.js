var app = angular.module("myApp", ["ui.router","ui.bootstrap"]);
app.config(['$stateProvider',function($stateProvider, $urlRouterProvider) {
         
    $stateProvider
     .state('index', {
        url: "",
        templateUrl: "view/login.html",
        controller:"login"
    })
    
    .state('dasboard',{
        url:"/dasboard",
        templateUrl:"view/dasboard.html",
        redirectTo: 'dasboard.aliados',
    })
    .state('dasboard.aliados',{
        url:"/aliados",
        templateUrl:"view/aliados.html",
        controller: "aliados"
    })
    .state('dasboard.directos',{
        url:"/directos",
        templateUrl:"view/directos.html",
        controller: "Directos"
    })
    
    $stateProvider.state('logout', {
        templateUrl: "view/login.html",
    })
        
}]);