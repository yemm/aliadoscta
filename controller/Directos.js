app.controller("Directos", function ($scope,$filter) {
    $scope.showDirectos = false;
    $scope.showDirectos1 = false;
    $scope.showDirectos2 = false;
  
    $scope.showcrear = function(){

      $scope.showDirectos1 = true;
      $scope.showDirectos = true;
      console.log($scope.showDirectos);
    }
    $scope.cancelar = function(){
        
        $scope.name = null;
        $scope.identification = null;
        $scope.lastname = null;
        $scope.email = null;
        $scope.age = null;
        $scope.company = null;
        $scope.directionco = null;
        $scope.quarters = null;
        $scope.charge = null;
        $scope.city = null;
        $scope.dateIn = null;
        $scope.management = null;
        $scope.area = null;  
        $scope.Invalid = false;
      
      $scope.showDirectos1 = false;
      $scope.showDirectos = false;

      console.log($scope.showDirectos);
    }
  
    $scope.showedit = function(){

        $scope.showDirectos2 = true;
        $scope.showDirectos = true;
        console.log($scope.showDirectos1);
    }
    $scope.cancelar1= function(){
        $scope.showDirectos = false;
        $scope.showDirectos2 = false;

        console.log($scope.showDirectos)
    }
    $scope.Directos = [];
    
    $scope.NuevoDir = function (){
        
        $scope.Invalid = false ;
        
    if(!$scope.formuD.nameD.$valid || !$scope.formuD.documentD.$valid || !$scope.formuD.lastnameD.$valid || 
    !$scope.formuD.emailD.$valid ||  !$scope.formuD.edadD.$valid ||!$scope.formuD.companyD.$valid || 
    !$scope.formuD.directioncoD.$valid ||!$scope.formuD.sedeD.$valid ||!$scope.formuD.cargoD.$valid || 
    !$scope.formuD.cityD.$valid || !$scope.formuD.DateinD.$valid || !$scope.formuD.gerenciaD.$valid ||!$scope.formuD.regionD.$valid  
    ){


        $scope.Invalid = true ;    
           
        }else{
          
        
        $scope.Directos.push({
            
        nombre:$scope.name,
        cedula:$scope.identification,
        apellido:$scope.lastname,
        correo:$scope.email,
        edad:$scope.age,
        empresas:$scope.company,
        direccionComite:$scope.directionco,
        sede:$scope.quarters,
        cargo:$scope.charge,
        ciudad:$scope.city,
        fechaIngreso:$scope.dateIn,
        gerencia:$scope.management,
        region:$scope.area,
    
        });

        console.log($scope.Directos.push);
        console.log($scope.formuD);
        
                
        $scope.showDirectos = false;
        $scope.showDirectos1 = false;
        
        $('#modalDirecto').modal('show');
        
    }
    
    }
    
        $scope.currentPage = 0;
        $scope.pageSize = 9;
        $scope.Directos = [];
    
        $scope.getDirectos = function () {
    
        return $filter('filter')($scope.Directos)
    
        }
    
        $scope.numberOfPages=function(){
            
        return Math.ceil($scope.getDirectos().length/$scope.pageSize);
        
        }  
    });

        app.filter('startFrom', function() {
            return function(input, start) {
            start = +start; //parse to int
            return input.slice(start);
        }
 
});


